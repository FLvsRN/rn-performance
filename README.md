
# Calc app - React Native

An application that uses several algorithms to calculate Fibonacci sequence numbers.

## Algorithms

### Binet's formula

<img src="https://latex.codecogs.com/svg.image?F_n&space;=&space;\frac{(1&plus;\sqrt{5})^n&space;-&space;(1-\sqrt{5})^n}{2^n\sqrt{5}}" title="F_n = \frac{(1+\sqrt{5})^n - (1-\sqrt{5})^n}{2^n\sqrt{5}}" />

### Recursive algorithm

        1.  FR(n: Int) :Int 
        2.      if n == 0 
        3.          return 0
        4.      if n == 1
        5.          return 1
        6.      return FR(n-1) + FR(n-2)

### Recursive algorithm with memorization

        1.  Array A[]
        2.  
        3.  FRM(n: Int) :Int
        4.      if n == 0
        5.          return 0
        6.      if n == 1
        7.          return 1
        8.      else if n ≤ A.length
        9.          return A[n-1]
        10.     else
        11.         temp = FRM(n-1) + FRM(n-2)
        12.         A.insert(temp)
        13.         return temp



## License
[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).

[![License: CC BY-SA 4.0](https://licensebuttons.net/l/by-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-sa/4.0/)
