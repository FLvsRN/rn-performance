export class Calc {
	fibonacciBinetsFormula = (n) =>
		Math.round((Math.pow(1 + Math.sqrt(5), n) - Math.pow(1 - Math.sqrt(5), n)) / (Math.pow(2, n) * Math.sqrt(5)));

	fibonacciRecursive = (n) => {
		if (n === 0) return 0;
		else if (n === 1) return 1;
		else return this.fibonacciRecursive(n - 1) + this.fibonacciRecursive(n - 2);
	};

	fibonacciRecursiveArray = (num, cache = [ 0, 1 ]) => {
		if (num === 0) return 0;
		if (num === 1) return 1;
		else if (num <= cache.length) {
			return cache[num - 1];
		} else {
			let temp = this.fibonacciRecursiveArray(num - 1, cache) + this.fibonacciRecursiveArray(num - 2, cache);
			cache.push(temp);
			return temp;
		}
	};
}
