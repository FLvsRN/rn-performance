import React, { useState } from 'react';
import { StyleSheet, Text, Button, View } from 'react-native';

import { Calc } from './calc';

const input = [ 1, 7, 12, 23, 37, 40, 42 ];
const iterations = 10;

const App = () => {
	const [ res1, setRes1 ] = useState();
	const [ res2, setRes2 ] = useState();
	const [ res3, setRes3 ] = useState();

	pressFibonacciBinetsFormula = () => {
		console.log('#1 tracker benchmark...');
		let start = window.performance.now();

		for (let i = 0; i < iterations; i++) {
			this.runFibonacciBinetsFormula();
		}
		let end = window.performance.now();
		let execTime = end - start;
		let res = execTime / (input.length * iterations);
		console.log(`avg time per iteration ${res} ms`);
		setRes1(`${+res.toFixed(4)} ms`);
	};

	pressFibonacciRecursive = () => {
		console.log('#2 tracker benchmark...');
		let start = performance.now();
		for (let i = 0; i < 1; i++) {
			this.runFibonacciRecursive();
		}
		let end = performance.now();

		let execTime = end - start;
		let res = execTime / (input.length * iterations);

		console.log(`avg time per iteration ${res} ms`);
		setRes2(`${+res.toFixed(4)} ms`);
	};

	pressFibonacciRecursiveMemorization = () => {
		console.log('#3 tracker benchmark...');
		let start = window.performance.now();
		for (let i = 0; i < iterations; i++) {
			this.runFibonacciRecursiveMemorization();
		}
		let end = window.performance.now();
		let execTime = end - start;
		let res = execTime / (input.length * iterations);
		console.log(`avg time per iteration ${res} ms`);
		setRes3(`${+res.toFixed(4)} ms`);
	};

	runFibonacciBinetsFormula = () => {
		let calc = new Calc();
		for (let i = 0; i < input.length; i++) {
			calc.fibonacciBinetsFormula(input[i]);
		}
	};
	runFibonacciRecursive = () => {
		let calc = new Calc();
		for (let i = 0; i < input.length; i++) {
			calc.fibonacciRecursive(input[i]);
		}
	};
	runFibonacciRecursiveMemorization = () => {
		let calc = new Calc();
		for (let i = 0; i < input.length; i++) {
			calc.fibonacciRecursiveArray(input[i]);
		}
	};

	return (
		<View style={styles.mainContainer}>
			<View style={styles.itemContainer}>
				<Button title={"Fibonacci\n(Binet's formula)"} onPress={this.pressFibonacciBinetsFormula} />
				<Text>{res1}</Text>
			</View>
			<View style={styles.itemContainer}>
				<Button title={'Fibonacci\n(recursion)'} onPress={this.pressFibonacciRecursive} />
				<Text>{res2}</Text>
			</View>
			<View style={styles.itemContainer}>
				<Button
					title={'Fibonacci\n(recursion+memorization)'}
					onPress={this.pressFibonacciRecursiveMemorization}
				/>
				<Text>{res3}</Text>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	mainContainer: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center'
	},
	itemContainer: {
		margin: 10,
		alignItems: 'center'
	}
});

export default App;
