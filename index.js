/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './src/App';
import { name as performanceApp } from './app.json';

AppRegistry.registerComponent(performanceApp, () => App);
